const tableBody = document.getElementById('tableBody')
// const GEONAMES_USER = "rachelwong"
const OPENCAGE_API_KEY = "902656e04ef84abca2ba26a3425f6e98" // geocoding
const GMAP_API_KEY = "AIzaSyBt5BHOEVXxL8hlfUd3ZC_8I6k2NBEVgJE" // map

async function fetchData() {

  // get satellite
  let bogeyAPI = "https://data.ivanstanojevic.me/api/tle" // this returns one page of results (20)
  let bogeyResponse = await fetch(bogeyAPI)
  let bogeyData = await bogeyResponse.json()

  for (bogey of bogeyData.member) {
    let satelliteRes = {
      id: bogey.satelliteId,
      name: bogey.name,
      long: 0,
      lat: 0,
      location: "",
      date: bogey.date
    }

    let coordinates = convertTLE(bogey) // returns array [long, lat]

    if (coordinates != []) {
      satelliteRes.long = Number(coordinates[0]).toFixed(2)
      satelliteRes.lat = Number(coordinates[1]).toFixed(2)

      // get location
      let openCageAPI = 'https://api.opencagedata.com/geocode/v1/json' +
        '?' +
        'key=' + OPENCAGE_API_KEY +
        '&q=' + encodeURIComponent(satelliteRes.lat + ',' + satelliteRes.long) +
        '&pretty=1' +
        '&no_annotations=1'

      let locationResponse = await fetch(openCageAPI)
      let locationData = await locationResponse.json()

      // set location depending on whether nil, country or ocean
      if (locationData.results.length === 0) {
        satelliteRes.location = "No data"
      } else if (locationData.results[0].components.body_of_water) {
        satelliteRes.location = locationData.results[0].components.body_of_water
      } else if (locationData.results[0].components.country) {
        satelliteRes.location = locationData.results[0].components.country
      } else if (locationData.results[0].components.continent) {
        satelliteRes.location = locationData.results[0].components.continent
      }
    }

    // display data for each satellite
    dropMarker(satelliteRes) // on map
    displayTable(satelliteRes) // on table
  }
}

// Decode TLE into coordinates
function convertTLE(data) {
  let line1 = data.line1
  let line2 = data.line2
  let satrec = satellite.twoline2satrec(line1, line2)
  // Position_velocity is a key-value pair of ECI coordinates (x, y, z).
  // They are base results from which all other coordinates are derived.
  let positionAndVelocity = satellite.propagate(satrec, new Date())

  if (positionAndVelocity.position != undefined) {

    // modular functions from satellite-js to grab coordinates
    let positionEci = positionAndVelocity.position
    var gmst = satellite.gstime(new Date()) // GMST for coordinate transforms
    let positionGd = satellite.eciToGeodetic(positionEci, gmst)
    let longitude = positionGd.longitude
    let latitude = positionGd.latitude
    let longitudeStr = satellite.degreesLong(longitude)
    let latitudeStr = satellite.degreesLat(latitude)

    let coordinates = [longitudeStr, latitudeStr]
    console.log('coordinates:', coordinates)

    return coordinates

  } else {
    return [] // Position and Velocity values not available to calculate coordinates
  }
}

// Display data as table rows
function displayTable(tincan) {
  var {
    id,
    name,
    long,
    lat,
    date,
    location
  } = tincan

  let row = document.createElement('tr')

  let idCell = document.createElement('td')
  idCell.innerHTML = id

  let nameCell = document.createElement('td')
  nameCell.innerHTML = name

  let longCell = document.createElement('td')
  longCell.innerHTML = long

  let latCell = document.createElement('td')
  latCell.innerHTML = lat

  let locationCell = document.createElement('td')
  locationCell.innerHTML = location

  let dateCell = document.createElement('td')
  dateCell.innerHTML = date

  row.appendChild(idCell)
  row.appendChild(nameCell)
  row.appendChild(longCell)
  row.appendChild(latCell)
  row.appendChild(locationCell)
  row.appendChild(dateCell)

  tableBody.appendChild(row)
}

let map // initialise a google map

// called by google maps api in the <script> in html 
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 2,
    center: {
      lat: 0,
      lng: 0
    },
    streetViewControl: false
  })
}

// set each satellite as marker on google map
function dropMarker(tincan) {
  var {
    name,
    long,
    lat,
  } = tincan

  let location = {
    lat: parseFloat(lat), 
    lng: parseFloat(long)
  }

  let content = `<h4>${name}</h4>`
  let infoWindow = new google.maps.InfoWindow({
    content
  })

  let marker = new google.maps.Marker({
    position: location,
    map: map,
    title: name
  })

  marker.addListener('click', () => {
    infoWindow.open(map, marker)
  })

}