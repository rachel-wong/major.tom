let container = document.getElementById('content')
// const GEONAMES_USER = "rachelwong"
const OPENCAGE_API_KEY = "902656e04ef84abca2ba26a3425f6e98" // geocoding
const GMAP_API_KEY = "AIzaSyBgS88-ehuQvQEb-Yk7U5q6_SEmHcq9IA8" // map

function getData() {
  let api = "https://data.ivanstanojevic.me/api/tle" // this returns one page of results (20)
  fetch(api)
    .then(response => {
      return response.json()
    })
    .then(data => {
      let rawData = data.member
      for (item of rawData) {
        let id = item.satelliteId
        let name = item.name
        let line1 = item.line1
        let line2 = item.line2

        let satrec = satellite.twoline2satrec(line1, line2)
        // Position_velocity is a key-value pair of ECI coordinates (x, y, z).
        // They are base results from which all other coordinates are derived.
        let positionAndVelocity = satellite.propagate(satrec, new Date())

        if (positionAndVelocity.position != undefined) {

          // modular functions from satellite-js to grab coordinates
          let positionEci = positionAndVelocity.position
          var gmst = satellite.gstime(new Date()) // GMST for coordinate transforms
          let positionGd = satellite.eciToGeodetic(positionEci, gmst)
          let longitude = positionGd.longitude
          let latitude = positionGd.latitude
          let height = positionGd.height
          let longitudeStr = satellite.degreesLong(longitude)
          let latitudeStr = satellite.degreesLat(latitude)

          let location = getLocation(longitudeStr, latitudeStr)

          let bogey = {
            id,
            name,
            location,
            latitudeStr,
            longitudeStr,
            height
          }

          displayData(bogey)

        } else {
          console.log("Position and Velocity base xyz values not available for satellite " + id + " called " + name + " and so can't be mapped. ")
        }
      }
    })
    .catch(err => {
      console.error(err)
    })
}

function getLocation(long, lat) {

  // refer to opencage docs
  var api_url = 'https://api.opencagedata.com/geocode/v1/json'
  let geoAPI = api_url +
    '?' +
    'key=' + OPENCAGE_API_KEY +
    '&q=' + encodeURIComponent(lat + ',' + long) +
    '&pretty=1' +
    '&no_annotations=1'

  fetch(geoAPI)
    .then(res => {
      return res.json()
    })
    .then(data => {
      if (data.results[0].components.body_of_water) {
        console.log("ocean: ", data.results[0].components.body_of_water)
        return data.results[0].components.body_of_water
      } else if (data.results[0].components.country) {
        console.log("country: ", data.results[0].components.country)
        return data.results[0].components.country
      } else if (data.results[0].components.continent) {
        console.log("country: ", data.results[0].components.continent)
        return data.results[0].components.continent
      } else {
        console.log("undefined error", " lat: ", lat, " long: ", long, "data: ", data)
      }
    })
    .catch(err => {
      console.error(err)
    })
}

function displayData(tincan) {
  var {
    id,
    name,
    location, // country or ocean
    latitudeStr,
    longitudeStr,
    height
  } = tincan

  const card = document.createElement('div')
  card.setAttribute('class', 'card')

  let header = document.createElement('h3')
  header.innerHTML = "name: " + name + "(id: " + id + ")"

  let paragraph = document.createElement('p')
  paragraph.innerHTML = "Longitude: " + Number(longitudeStr).toFixed(2) + " Latitude: " + Number(latitudeStr).toFixed(2) + " Location: " + location

  card.appendChild(header)
  card.appendChild(paragraph)
  container.appendChild(card)
}