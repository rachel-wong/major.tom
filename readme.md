# :rocket: Major Tom

Tracking satellites using NASA's open source API the good ol' fashion vanilla JS-way.

![walkthru_gif](assets/majorTom.gif)

## :8ball: The APIs

**The satellite data** uses NASA's [TLE API](https://data.ivanstanojevic.me/api/tle/docs) which provides the two-line element set records for each satellite.

Plugging on, I thought it'd be nice to map out the **general locale** of where the satellite is flying over, be it continent or ocean. I started to use the free [geonames API](http://www.geonames.org/export/web-services.html#countrycode) but opted for [OpenCage](https://opencagedata.com/tutorials/geocode-in-javascript) which allows more requests per day. 

Last hurdle is to **map the coordinates** as markers onto a [Google Map API](https://developers.google.com/maps/documentation/javascript/tutorial).

Overall, a good project opportunity to understand *async/await* aspect of fetch API because all the displaying data are inter-dependent on each others' APIs responding on time. No satellite data? No coordinates, which means no location and no markers on map - no bueno.

## :cyclone: The Approach

The NASA API returns a TLE (Two Line Element) for each satellite which is a data format that needs to be decoded using the [Satellite-js](https://github.com/shashwatak/satellite-js) functions.

Once decoded, you have access to the name, id and coordinates can be mapped onto Google Maps as markers the old fashion way.

A quick and dirty job bootstrapping it all to get a presentable UI. The goal of the project is to understand the moving parts and working with APIs, not ruiminating over kerning and transitions.

## :construction: To-dos

* The NASA TLE api as it stands is shaped to return 20 results per call. There are nearly 8000 results in the server (a lot of junk up there). Will have to find a way to recursively call the server to get all the pages and all the latest data. So at the moment, it will only have around 20 data points.

* OpenCage api doesn't allow a lot of requests in practice. 

* Google Maps API degraded to dev-only. Not wanting to add billing details to Google Console nor do I want to create an organisation profile with Google cloud. Not sure what the workaround is yet. 

* Async/Await issue not resolved. Data is being populated sequentially, meaning each line of the table and each marker on the map appears one by one. Not sure whether this means restructuring the whole code or because my understanding of asynchronous fetch is off the mark. I thought I have done the right thing by encapsulating all the api calls into one async function and awaiting on all the responses.

## :eyes: Some observations

* Luckily all the APIs are free so no CORS! :see_no_evil:

* need to find a way to structure multiple api calls

## Other resources

[Satellite-js npm package](https://github.com/shashwatak/satellite-js)

[A jump-off reference](https://www.youtube.com/watch?v=8NUqDc1bQ84&t=980s)